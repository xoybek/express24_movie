package uz.micro.star.projecty_for_exprees24.dialogs

import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import uz.micro.star.projecty_for_exprees24.databinding.DialogLoaderBinding

class LoaderDialog(context: Context) : AlertDialog(context) {
    val binding = DialogLoaderBinding.inflate(LayoutInflater.from(context))

    init {
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setCancelable(false)
        setView(binding.root)
    }
}
package uz.micro.star.projecty_for_exprees24.di.modue

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import uz.micro.star.projecty_for_exprees24.di.scopes.ViewModelKey
import uz.micro.star.projecty_for_exprees24.viewmodels.MainActivityViewModel

@Module
abstract class MainViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun homeViewModel(mainActivityViewModel: MainActivityViewModel): ViewModel
}
package uz.micro.star.projecty_for_exprees24.network.models.main.genres


import com.google.gson.annotations.SerializedName

data class Genre(
    @SerializedName("id")
    var id: Int,
    @SerializedName("name")
    var name: String
)
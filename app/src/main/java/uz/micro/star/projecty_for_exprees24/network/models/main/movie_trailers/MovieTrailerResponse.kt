package uz.micro.star.projecty_for_exprees24.network.models.main.movie_trailers


import com.google.gson.annotations.SerializedName

data class MovieTrailerResponse(
    @SerializedName("results")
    var results: List<Result>
)
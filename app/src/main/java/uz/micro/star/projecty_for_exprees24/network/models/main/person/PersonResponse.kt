package uz.micro.star.projecty_for_exprees24.network.models.main.person


import com.google.gson.annotations.SerializedName

data class PersonResponse(
    @SerializedName("results")
    var results: List<PersonResult>,
)
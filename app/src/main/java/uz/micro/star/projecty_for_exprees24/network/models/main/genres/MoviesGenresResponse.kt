package uz.micro.star.projecty_for_exprees24.network.models.main.genres


import com.google.gson.annotations.SerializedName

data class MoviesGenresResponse(
    @SerializedName("genres")
    var genres: List<Genre>
)
package uz.micro.star.projecty_for_exprees24.di.modue

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import uz.micro.star.projecty_for_exprees24.viewmodels.DaggerViewModelFactory

/**
 * Created by Microstar on 19.08.2021
 */
@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactor(modelProviderFactory: DaggerViewModelFactory?): ViewModelProvider.Factory?
}
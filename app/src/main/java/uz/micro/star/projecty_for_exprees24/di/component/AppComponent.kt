package uz.micro.star.projecty_for_exprees24.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import uz.micro.star.projecty_for_exprees24.app.BaseApplication
import uz.micro.star.projecty_for_exprees24.di.modue.ActivityBuildersModule
import uz.micro.star.projecty_for_exprees24.di.modue.AppModule
import uz.micro.star.projecty_for_exprees24.di.modue.ViewModelFactoryModule
import javax.inject.Singleton

/**
 * Created by Microstar on 19.08.2021
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityBuildersModule::class,
        ViewModelFactoryModule::class,
    ]
)
interface AppComponent : AndroidInjector<BaseApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}

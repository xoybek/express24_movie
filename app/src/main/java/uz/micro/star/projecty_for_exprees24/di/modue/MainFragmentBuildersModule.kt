package uz.micro.star.projecty_for_exprees24.di.modue

import dagger.Module
import dagger.android.ContributesAndroidInjector
import uz.micro.star.projecty_for_exprees24.fragments.ActorDetailFragment
import uz.micro.star.projecty_for_exprees24.fragments.MovieDetailFragment
import uz.micro.star.projecty_for_exprees24.fragments.SplashFragment
import uz.micro.star.projecty_for_exprees24.fragments.main.MainFragment

/**
 * Created by Microstar on 19.08.2021
 */
@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun homeFragment(): SplashFragment

    @ContributesAndroidInjector
    abstract fun actorFragment(): ActorDetailFragment

    @ContributesAndroidInjector
    abstract fun mainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun movieDetailFragment(): MovieDetailFragment
}
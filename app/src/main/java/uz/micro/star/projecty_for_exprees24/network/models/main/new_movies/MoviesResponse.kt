package uz.micro.star.projecty_for_exprees24.network.models.main.new_movies


import com.google.gson.annotations.SerializedName

data class MoviesResponse(
    @SerializedName("results")
    var results: List<MovieResult>,
)
package uz.micro.star.projecty_for_exprees24.network.models.main.movie_detail


import com.google.gson.annotations.SerializedName

data class Genre(
    @SerializedName("id")
    var id: Int,
    @SerializedName("name")
    var name: String
)
package uz.micro.star.projecty_for_exprees24.viewmodels

import androidx.lifecycle.ViewModel
import uz.micro.star.projecty_for_exprees24.repositories.MovieRepository

import javax.inject.Inject

/**
 * Created by Microstar on 19.08.2020.
 */

class MainActivityViewModel @Inject constructor(
    var movieRepository: MovieRepository
) : ViewModel() {
    var cardNumber: String = ""
}

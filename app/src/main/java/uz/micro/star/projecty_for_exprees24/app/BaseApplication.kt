package uz.micro.star.projecty_for_exprees24.app

import dagger.android.support.DaggerApplication
import uz.micro.star.projecty_for_exprees24.di.component.DaggerAppComponent


/**
 * Created by Microstar on 19.08.2021
 */
class BaseApplication : DaggerApplication() {
    override fun applicationInjector() = DaggerAppComponent.builder().application(this).build()
}